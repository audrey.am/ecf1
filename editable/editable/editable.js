var json = "../data/points.json";
let monTab;

fetch(json) // j'envoie ma requête
  .then(
    // quand je reçois la réponse
    (response) => response.json() // je récupère le JSON de la réponse
  )
  .then(
    //! quand c'est pret
    (tab) => {
      monTab = tab;
      // je mets mes data dans ma variable
      creerMonTableauHtml(monTab); // je lance ma fonction d'affichage
    }
  )
  .catch((err) => {
    console.log(err); // j'affiche les erreurs HTTP au cas où
  });

function creerMonTableauHtml(jsonObj) {
  var table = document.createElement("table");
  table.id = "table";
  table.className = "table";
  body.appendChild(table);
  var entete = document.createElement("div");
  entete.id = "entete";
  entete.className = "entete";
  table.appendChild(entete);
  var nomDuFichier = document.createElement("h2");
  nomDuFichier.className = "nomDuFichier";
  entete.appendChild(nomDuFichier);
  nomDuFichier.textContent = json;
  var recherche = document.createElement("input");
  recherche.id = "cherche";
  recherche.className = "cherche";
  recherche.onkeyup = cherche;
  entete.appendChild(recherche);
  var btnPlus = document.createElement("button");
  btnPlus.id = "ajoute";
  btnPlus.innerText = "+";
  btnPlus.addEventListener("click", ajouterUneLigne);
  entete.appendChild(btnPlus);
  var save = document.createElement("button");
  save.id = "save";
  save.innerText = "Save";
  save.addEventListener("click", sauvegarder);
  entete.appendChild(save);

  var tableDeValeur = document.createElement("thead");
  tableDeValeur.id = "tableDeValeur";
  tableDeValeur.className = "tableDeValeur";
  table.appendChild(tableDeValeur);

  var tableDeDonnee = document.createElement("div");
  tableDeDonnee.id = "tableDeDonnee";
  tableDeDonnee.className = "tableDeDonnee";
  table.appendChild(tableDeDonnee);

  for (let cle in monTab[0]) {
    //keys
    var cleDuTableau = document.createElement("div");
    cleDuTableau.id = "colonne" + cle;
    cleDuTableau.className = "colonne" + cle;
    cleDuTableau.innerHTML = cle;
    var boutonTriDeLaCle = document.createElement("button");
    boutonTriDeLaCle.id = cle;
    boutonTriDeLaCle.className = "bouttontri";
    boutonTriDeLaCle.innerHTML = "Tri";
    boutonTriDeLaCle.addEventListener("click", trier);
    var modelCategorie = document.createElement("tr");
    modelCategorie.className = "modelCategorie";
    modelCategorie.id = "modelCategorie";
    tableDeValeur.appendChild(modelCategorie);
    modelCategorie.appendChild(cleDuTableau);
    modelCategorie.appendChild(boutonTriDeLaCle);
  }
  for (let ligne of monTab) {
    //ligne
    var ligneDeValeur = document.createElement("tr");
    ligneDeValeur.className = "resultat";
    ligneDeValeur.id = "res" + ligne["id"];
    tableDeDonnee.appendChild(ligneDeValeur);
    for (let donnee in ligne) {
      //donnee

      var donneeDansLigne = document.createElement("input");
      donneeDansLigne.size = 20;
      donneeDansLigne.id = "data" + donnee + ligne["id"];
      donneeDansLigne.className = "data" + donnee + ligne["id"];
      donneeDansLigne.value = ligne[donnee];
      donneeDansLigne.name = ligne[donnee];
      ligneDeValeur.appendChild(donneeDansLigne);
    }
  }
}

function trier(e) {
  monTab.sort((a, b) =>
    a[e.target.id].toString().localeCompare(b[e.target.id].toString())
  );
  var el = document.getElementById("table");
  el.remove();
  creerMonTableauHtml();
}
function cherche(e) {
  let entree = document.getElementById("cherche").value;
  for (i = 0; i < monTab.length; i++) {
    let resultatDeRecherche = document.getElementById("res" + i);
    if (!resultatDeRecherche.innerHTML.includes(entree)) {
      resultatDeRecherche.style.display = "none";
    } else {
      resultatDeRecherche.style.display = "list-item";
    }
  }
}
let btnAjoute = document.getElementById("ajoute");

function ajouterUneLigne(e) {
  var ligneACloner = document.getElementById("res0");
  var nouvelledonnee = ligneACloner.cloneNode([true]);
  nouvelledonnee.childNodes.forEach((e) => (e.value = ""));
  table.appendChild(nouvelledonnee);
}

async function sauvegarder(e) {
  const reponseDeSauvegarde = await fetch("../test/up.php", {
    method: "POST",
    body: JSON.stringify(monTab),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((reponse) => reponse.json())
    .catch((err) => {
      console.log(err);
    });
  console.log(reponseDeSauvegarde);
}
