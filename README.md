# Editable

1. Informations
2. Langage
3. Installation
4. Fonctionnalités
---
---
1. Informations

Editable est un tableau éditable qui permet de gerer des tables à base de fichiers Json

---
2. Langage

Editable utilise du Javascript

---

3. Comment l'installer?

Récuperer le clone du dépot Git avec l'adresse [***Git Lab***](https://gitlab.com/audrey.am/ecf1)
Inserer le tableau Json dans le fichier DATA
et modifier le fichier d'accès du fichier dans la page _editable.js_
```js
1 var json = "../data/votrefichier.json";
```

---
4. Fonctionnalités

Affichage des données d'un tableau JSON
Filtre de données
Tri de données sur chaque colonne 
Modification des données
Ajout de ligne
-Sauvegarde-
